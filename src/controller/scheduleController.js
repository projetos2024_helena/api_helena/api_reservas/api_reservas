const connect = require("../db/connect");

module.exports = class scheduleController {
  static async createShedule(req, res) {
    const { dateStart, dateEnd, days, user, classroom, timeStart, timeEnd } =
      req.body;

    if (
      !dateStart ||
      !dateEnd ||
      !days ||
      !user ||
      !classroom ||
      !timeStart ||
      !timeEnd
    ) {
      return res
        .status(400)
        .json({ error: "Todos os campos devem ser preenchidos" });
    }
    const daysString = days.map((day) => `${day}`).join(",");

    //verificando se ja não existe uma reserva
    try {
      const overlapQuery = `SELECT * FROM schedule 
          WHERE classroom = '${classroom}' 
          AND(
            (dateStart<='${dateEnd}' AND dateEnd >='${dateStart}')
          )AND(
                (timeStart<='${timeEnd}' AND timeEnd >='${timeStart}')
        ) AND( 
            (days LIKE '%Seg' AND '${daysString}' LIKE '%Seg%')OR
            (days LIKE '%Ter' AND '${daysString}' LIKE '%Ter%')OR
            (days LIKE '%Qua' AND '${daysString}' LIKE '%Qua%')OR
            (days LIKE '%Qui' AND '${daysString}' LIKE '%Qui%')OR
            (days LIKE '%Sex' AND '${daysString}' LIKE '%Sex%')OR
            (days LIKE '%Sab' AND '${daysString}' LIKE '%Sab%')
                
          )`;

      connect.query(overlapQuery, function (err, results) {
        if (err) {
            console.log(err)
          return res.status(500).json({ error: "erro ao verificar agendamento" });
        }
        // se houver resultado a consulta, ja existe agendamento
        if (results.length > 0) {
          return res
            .status(400)
            .json({ error: "sala ocupada para os mesmos dias e horarios" });
        }

        // caso a query nao retorne nada inserimos na tabela
        const insertQuery = `INSERT INTO schedule (dateStart, dateEnd, days, user, classroom, timeStart, timeEnd) 
        VALUES(
            '${dateStart}', 
            '${dateEnd}',
            '${daysString}',
            '${user}',
            '${classroom}',
            '${timeStart}',
            '${timeEnd}'
        ) `;
        // executando a query de inserção 
        connect.query(insertQuery, function(err){
            if (err) {
                console.log(err)
              return res.status(500).json({ error: "erro ao cadastrar agendamento" });
              }
              return res.status(201).json({message:"agendamento cadastrado com sucesso"});

        });
      });
    } catch (error) {
        console.log("erro ao executar a consulta:", error);
        res.status(500).json({error:"erro interno de servidor"})
    }
  }
};
