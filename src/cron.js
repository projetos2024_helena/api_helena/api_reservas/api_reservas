const cron = require("node-cron");
const cleanUpSchedulesServices = require ("./services/cleanUpSchedulesServices");

// Agendamento da limpeza 

cron.schedule("*/30 * * * * *",async()=>{
    try{
       await cleanUpSchedulesServices();
       console.log("Limpeza automática executada")
    }catch (error){
       console.error("Erro ao executar limpeza automática", error)
    }
})